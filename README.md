# Attendance Add-Ons for Modern Tribe's The Events Calendar WordPress plugin.

This is a WordPress plugin that extends functionality for another plugin - [The Events Calendar by Modern Tribe](https://theeventscalendar.com/product/wordpress-events-calendar/).  The Events Calendar adds event registration and management capabilities to WordPress.  

This attendance plugin adds capabilities for managing and viewing event registrations, including:

* Ability for admins to enroll a user in an event
* List all event registrations for a specific user.
* See and access contextual information for an attendee on the event attendance page.

