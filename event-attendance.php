<?php
/**
 * @package Event_Attendance
 * @version 0.4
 */
/*
Plugin Name: Event Attendance
Plugin URI: https://gitlab.uvm.edu/jhenry/event-attendance
Description: Additional functionality and modifications to Event Calendar plugin.
Author: Justin Henry
Version: 0.4
Author URI: https://uvm.edu/~jhenry
*/



add_filter( 'event_tickets_attendees_table_primary_info_column', 'link_attendee_to_user', 10, 2 );
add_action('admin_menu', 'user_attendance_submenu' ); 
add_action( 'admin_post_add_attendee', 'do_add_single_attendee' );
add_action( 'admin_post_add_multiple_attendees', 'do_add_multiple_attendees' );
add_action( 'event_tickets_rsvp_tickets_generated_for_product', 'update_registration_user', 10, 3 );

add_action('tribe_events_tickets_attendees_ticket_sales_bottom', 'attendance_page_enrollment_form' ); 
add_action('tribe_tickets_attendees_event_details_list_top', 'show_event_title_on_attendance_screen' ); 
/**
 * Hook into tribe event attendance table to display more info about attendee.
 * For example, add a link to the user's attendance page 
 *
 *
 * @param string $output
 * @param array  $item
 */
function link_attendee_to_user($output, $item) {
	$purchaser_name  = empty( $item[ 'purchaser_name' ] ) ? '' : esc_html( $item[ 'purchaser_name' ] );
	$purchaser_email = empty( $item[ 'purchaser_email' ] ) ? '' : esc_html( $item[ 'purchaser_email' ] );

	$post_object = get_post( $item ['order_id'] );
	$user_id = $post_object->post_author;	
	
	$affiliation = get_user_meta($user_id, 'eduPersonPrimaryAffiliation', true);
	$affiliation = $affiliation . " | " . get_user_meta($user_id, 'ou', true);

	$purchaser_user_link = get_edit_user_link( $user_id );
	$purchaser_attendance_link = get_user_attendance_url ( $user_id );

	$output = "
                        <a class='purchaser_name' href='{$purchaser_user_link}'>{$purchaser_name}</a>
                        <br /> {$affiliation}
                        <br /> <a class='purchaser_email' href='mailto:{$purchaser_email}'>{$purchaser_email}</a>
                        <br /> <a class='purchaser_events' href='{$purchaser_attendance_link}'>View All Registrations</a>
			
                ";

	return $output;
}


/**
 * Show the event title when looking at enrollments.
 *
 */
function show_event_title_on_attendance_screen($event_id) {
		
	$event = get_post($event_id);
	echo "<h3>" . $event->post_title . "</h3>";
}
/**
 * Allow enrollment creation from the attendee page.
 *
 */
function attendance_page_enrollment_form($event_id) {
		
		include 'form-multi-enroll.php';
}

/**
 * Display a table of events that the user has registered for.  
 * Present forms for adding a user to an event, and for
 * switching to view a different user's event registrations.
 *
 */
function show_events_for_user() {
	

	// If no user is set, present a form to select one
	if ( !isset( $_GET["user_id"] ) ) {
		echo "<h1>Past & Present Registrations</h1>";	 	

		include 'form-select-user.php';
	}
	else {
		$username = $_GET["user_id"];
		$user = get_user_by('login', $username);
		$display_name = $user->display_name ?? $username;
	    	echo "<h1>Event Attendance/Registrations for " . $display_name . "</h1>";

		include 'form-select-user.php';

		// Set up and display table of registered events for this user
		$registrations = get_all_registrations_by_user($user->ID);
		$events = collect_registered_event_data ( $registrations ); 
		display_user_registrations($events);
	}
}

/**
 * Build the URL for a user's events registration page.
 *
 *
 * @param string $user_id
 * @param array  $item
 */
function get_user_attendance_url ( $user_id ) {
	$params = '?page=user-attendance&user_id=' . $user_id;
	$url = admin_url( $params, 'https' );
	
	return $url;

}

/**
 * Form action to add multiple, comma separated users to an event.
 *
 */
function do_add_multiple_attendees() {
	
	// get event post & meta
	$users = explode( ",", $_POST['user_ids']);
	$event = get_post( $_POST['event_id'] );
	
	foreach ($users as $username) {
		if (username_exists( $username ) ) {
			$user = get_user_by('login', $username );
		}
		else {
			// make a new user 
			$user_email = $username . "@uvm.edu";
			$random_password = wp_generate_password( 12, false );
			$user_id = wp_create_user( $username, $random_password, $user_email);

			// and update their profile meta 
			update_user_profile($user_id);	

			$user = get_user_by('id', $user_id );
		}
		
		add_attendee( $user, $event );	
	}

	// Send them back to the attendance page for the event
	$event_attendance_url = tribe_get_event_link( $event->ID );

	$roster_params = "edit.php?post_type=tribe_events&page=tickets-attendees&event_id=" . $event->ID;
	$event_roster_url = admin_url( $roster_params, 'https' );
	wp_redirect( $event_roster_url ); 
	exit;

}

/**
 * Action for form to add a single attendee to an event.
 *
 */
function do_add_single_attendee() {
	
	// get event post & meta
	$user = get_userdata( $_POST['user_id'] );
 	$event = get_post( $_POST['event_id'] );

	add_attendee($user, $event);	

	// Send them back to the attendance page for the user
	$attendance_url = get_user_attendance_url ( $user->ID ); 
	wp_redirect( $attendance_url ); 
	exit;

}

/**
 * Add an attendee to an event.  
 *
 */
function add_attendee($user, $event) {

	$event_meta = get_post_meta( $event->ID );

	// Should probly put this data into the form 
	// and just use parse_attendee_details();	
	$tribe_rsvp = new Tribe__Tickets__RSVP();
	$attendee_details =  array( 
	   'order_id' => $tribe_rsvp->generate_order_id(),
	   'full_name' => $user->user_nicename,
	   'email' => $user->user_email,
	   'order_status' => 'yes',
	   'optout' => true
	);

	$tickets = $tribe_rsvp->get_tickets_ids( $event->ID );

	// Need better validation.  We don't use more 
	// than one ticket per event, but still...
	$product_id = $tickets[0];

	$tribe_rsvp->generate_tickets_for( $product_id, 1, $attendee_details );
	

}
/**
 * Update post_author after a registration happens.
 * Otherwise, author of the order post is set to 
 * whomever is logged in, as opposed to the attendee.
 * 
 * 
 * @param int $product_id
 * @param int $order_id
 * @param int $qty
 */ 

function update_registration_user($product_id, $order_id, $qty){

	// Get the attendee that was just created by generate_tickets
	$attendees = tribe_tickets_get_attendees($order_id);
	$tribe_rsvp = new Tribe__Tickets__RSVP();
	$registration = $attendees[0];
	$user = get_user_by('email', $registration['purchaser_email']);
	
	// Update the post author for the newly generated registration
	$the_post = array(
		'ID'           => $registration['order_id'],
		'post_author'   => $user->ID
	);
	wp_update_post( $the_post );
}

/**
 * Find all post types that are both a RSVP and have the specified author.
 * 
 * 
 * @param string $user_id
 */
function get_all_registrations_by_user($user_id) {

	$post_args = array(
		'post_type'   => 'tribe_rsvp_attendees',
		'author' => $user_id
	);

	$registrations = get_posts( $post_args );
	return $registrations;
}

/**
 * Set up the table which is used to display a user's registrations.
 * 
 * 
 * @param array $registrations
 * @param array $events
 */
function collect_registered_event_data ($registrations) {

	foreach ( $registrations as $registration ) {
		$post_meta = get_post_meta( $registration->ID );
		//$event_id = $post_meta['_tribe_rsvp_event'][0];
		$event_post = get_post( $post_meta['_tribe_rsvp_event'][0] );

		$checked_in = get_check_in_status( $post_meta ); 
		$links_to_event = build_event_admin_links( $event_post->ID );
		$event_contacts = build_organizer_contacts( $event_post->ID ); 

		$event['checked_in'] = $checked_in;
		$event['event_date'] = tribe_get_start_date( $event_post->ID );
		$event['event_id'] = $event_post->ID;
		$event['event_contacts'] = $event_contacts;
		$event['event_name'] = $event_post->post_title . "<br />" . $links_to_event['roster_link'] . " | " . $links_to_event['event_link'];
		$events[] = $event;
	}
	
	return $events;
}


/**
 * Build out a list of comma separated organizers and event authors.  
 * 
 * 
 * @param int $event_id
 * @param string $event_contacts
 */
function build_organizer_contacts( $event_id ) {

		// Build contacts as basic comma separated list
		$organizer = tribe_get_organizer($event_id);
		$event_author = get_userdata($event_post->post_author);
		$event_contacts = $event_author->user_nicename . ", " . $organizer;

		return $event_contacts;
}

/**
 * Determine if the person has been marked as "attended" for the event.
 * 
 * 
 * @param array $post_meta
 * @param string $checked_in
 */
function get_check_in_status( $post_meta ) {

		// Find out if they showed up
		if (isset($post_meta['_tribe_rsvp_checkedin'][0])) {
			$checked_in = "Yes"; 
		} else {
			$checked_in = "No"; 
		}

		return $checked_in;
}

/**
 * Set up links to edit the event and view attendance for each 
 * of the user's registered events.
 * 
 * 
 * @param int $event_id
 * @param string $event
 */
function build_event_admin_links($event_id) {

		// Post & Roster links
		$edit_event_url = get_edit_post_link( $event_id );
		$roster_params = "edit.php?post_type=tribe_events&page=tickets-attendees&event_id=" . $event_id;
		$event_roster_url = admin_url( $roster_params, 'https' );
		$event['roster_link'] = '<a href="'. $event_roster_url . '">Roster</a>';
		$event['event_link'] = '<a href="'. $edit_event_url . '">Event</a>';
		
		return $event;

}

/**
 * Set up and display the table class for a user's registrations.
 * 
 * @param array $events
 */
function display_user_registrations($events) {

	include 'attendance-list-table.php';
	$table_maker = new Attendance__List_Table();
	$table_maker->items = $events;
	$table_maker->prepare_items();
	include 'display-registered-events.php';
}

/**
 * Add a menu item and a URL path for the user's registration page.
 * 
 */
function user_attendance_submenu(){
	add_menu_page( 'User Attendance', 'Attendance', 'manage_options', 'user-attendance', 'show_events_for_user' );
}


?>
