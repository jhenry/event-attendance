<form method="post" action="admin-post.php">
	<input type="hidden" name="action" value="add_attendee">
    <input type="hidden" name="user_id" id="enroll-user-id" value="<?php echo $user_id; ?>" />
<select name="event_id" id="enroll-event-id" /> <option value="">-- Enroll this user in an event: --</option>
<?php
	foreach ($events as $event) {
		echo '<option value="'. $event->ID . '">' . esc_html( $event->post_title ) . '</option>';
	}
?>
<?php submit_button( __( 'Enroll' ), 'small', 'enroll_user', false ) ?>
</form>
