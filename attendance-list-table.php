<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Attendance__List_Table extends WP_List_Table {

	public function get_columns() {
		$table_columns = array(
			'cb' => '<input type="checkbox" />', 
			'event_id' => 'Event ID',
			'event_name' => 'Event Name',			
			'event_date' => 'Date',			
			'checked_in' => 'Attended?',			
			'event_contacts' => 'Primary Contact(s)'			
		);		

		return $table_columns;
	}
	public function prepare_items() {
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = array();
		$this->_column_headers = array($columns, $hidden, $sortable);
	}
	public function column_default($item, $column_name) {
	    return $item[$column_name];
	}

}

?>
